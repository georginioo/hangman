import random

words = ["Apple","Banana","Orange","Pineapple","Tomato"]
alphabet = "abcdefghijklmnopqrstuvwxyz"
alphabet_list = list(alphabet)
guessedLetters = []
lives = 5

# Removes a guessed letter from the alphabet list
def removeLetter(letter, alphabet_list):
  alphabet_list = alphabet_list.replace(letter.lower(),"_")
  print("The remaining letters are:   "+alphabet_list)
  return alphabet_list

# Checks if the user has guessed the whole word
def checkGuessed(display_word):
  if "_" in display_word:
    guessed = False
  else:
    guessed = True
  return guessed

# Gets the chosen word and replaces each letter with _
def getDisplayWord(word):
  newWord = []
  for i in word:
    newWord.append("_")
  return newWord

# Replaces a _ in the displayWord with the correct letter
def addToDisplay(displayWord,letter,chosenWord):
  word_list = list(chosenWord.upper())
  letter_positions = [i for i, x in enumerate(word_list) if x == letter.upper()]
  
  for i in range(0,len(letter_positions)):
    displayWord[letter_positions[i]] = letter.upper()
  return displayWord

# Make sure input is one character and a letter
def validateGuess(guess,alphabet):
  lst = list(alphabet)
  if len(guess) == 1:
    if guess.lower() in guessedLetters:
      print("You have already guessed this letter!")
      return False
    else:
      if (ord(guess.lower()) >= 97) and (ord(guess.lower()) <= 122):
        guessedLetters.append(guess.lower())
        return True # Valid Entry
      else:
        print("Your entry is not a letter!")
        return False
  else:
    print("Your entry must be ONE character long.")
    return False

# Main code
play = True
while play == True:
  chosenWord = words[random.randrange(0,len(words))]
  displayWord = getDisplayWord(chosenWord)

  guessed = False
  while guessed == False:
    print(" ".join(displayWord))
    valid = False
    while valid == False:
      guess = str(input("\nEnter your guess: "))
      valid = validateGuess(guess,alphabet)

    if guess.upper() in chosenWord.upper():
      print("\nWell done, you guessed a letter.")
      alphabet = removeLetter(guess, alphabet)
      print("You have "+str(lives)+" lives remaining.")
      displayWord = addToDisplay(displayWord,guess,chosenWord)
    else:
      lives -= 1
      if lives > 0:
        print("\nIncorrect Guess. You have "+str(lives)+" lives remaining.")
      else:
        print("\nGAME OVER. The word was "+chosenWord)
        guessed = True
        break
      removeLetter(guess,alphabet)

    guessed = checkGuessed(displayWord)
    if guessed == True:
      print("\nWell Done! You guessed the word!\n")

  inp = input("Would you like to play again? Y/N")
  if inp.upper() == "Y":
    play = True
    guessed = False
  else:
    play = False
    
print("\nThanks for playing!")